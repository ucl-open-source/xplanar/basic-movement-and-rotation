# Basic movement and rotation

## Introduction
This project aims to showcase how to perform basic movements and rotation with a mover.

The program uses a statemachine structure to showcase different movements and rotations. When run, the specific movement/rotation performed is controlled by a variable named `testStartState` of the program. The available movements/rotations and their corresponding value for `testStartState` can be found in the following table:

| `testStartState` | Description |
|:---:|:---:|
| 100 | Rotating in axis A between negative and positive angle defined by the variable `target_a`. |
| 200 | Rotating in axis B between negative and positive angle defined by the variable `target_b`. |
| 300 | Alternating rotation with axis A and B between negative and positive angles defined by the variables `target_a` and `target_b`. |
| 400 | Alternating rotation with axis A and C between negative and positive angles defined by the variables `target_a` and `target_c`. |
| 500 | Synchronous rotation with axis A and C between negative and positive angles defined by the variables `target_a` and `target_c`. |
| 600 | Rotation with axis A between negative and positive angles defined by the variable `target_a` while spinning in axis C. |
| 700 | Movement in XY axis with rotation defined by sub state variable `movePosSubTest`. |

| `movePosSubTest` | Description |
|:---:|:---:|
| 0 | No rotation applied before movement |
| 1 | Rotation in axis A applied before movement defined by `target_a`. |
| 2 | Rotation in axis C applied before movement defined by `target_c`. |
| 3 | Updates target positions with a rotation in axis C. |



## Results